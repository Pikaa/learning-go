package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
)

func main() {
	fmt.Printf("SHA1 me!\n")
	fmt.Printf("%x\n", sha1.Sum([]byte("SHA1 me!")))
	fmt.Printf("SHA256 me!\n")
	fmt.Printf("%x\n", sha256.Sum256([]byte("SHA256 me!")))
	fmt.Printf("SHA512 me!\n")
	fmt.Printf("%x\n", sha512.Sum512([]byte("SHA256 me!")))
	fmt.Printf("Base64 me!\n")
	fmt.Printf("%x", base64.StdEncoding.EncodeToString([]byte("Base64 me!")))
}
